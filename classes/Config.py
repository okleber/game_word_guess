import json

class GameConfig():
    language=str()
    category=str()
    word=str()


    def set_language(self):
        return 0

    def list_languages(self):
        data=self.open_data_file()
        return sorted(data['language'].keys())

    def open_data_file(self):
        f=open('words.json','r')
        data=json.loads(f.read())
        f.close()
        return data